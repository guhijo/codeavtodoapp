package codeavenue.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import codeavenue.pageObjects.HomePage;
import codeavenue.pageObjects.LoginPage;

public class LoginTest extends AbstractTest {
	
	LoginPage loginPage;
	HomePage homePage;
	
	@Before
	public void setUp(){
		driver = AbstractTest.getDriver("firefox", "");
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.login(TEST_USER,TEST_PASS);
		homePage = PageFactory.initElements(driver, HomePage.class);
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
	
	@Test
	public void successfulLogin() {
		
		assertEquals("Signed in successfully.", "Signed in successfully.", 
				homePage.getLoginMessage());
	}	
}
