package codeavenue.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;

public abstract class AbstractTest {
	//-----------Those login information could be loaded from a properties file
	protected static final String TEST_USER = "guhijo@gmail.com";
	protected static final String TEST_PASS = "test4now";
	//----------------------------------------------------------------
	static WebDriver driver;
	static String baseUrl = "http://qa-test.avenuecode.com/users/sign_in";
	
	public AbstractTest() {
				
	}

	public static WebDriver getDriver(String driverName, String driverLocation) {
		switch(driverName) {
		case "ie":
			driver = new InternetExplorerDriver();
			break;
		case "chrome":
			System.setProperty("webdriver.chrome.driver", driverLocation);
			driver = new ChromeDriver();
			break;
		default:
			ProfilesIni profile = new ProfilesIni();
		    FirefoxProfile myprofile = profile.getProfile("Testing");
		    System.out.println("Starting browser");
		    driver = new FirefoxDriver(myprofile);
		    System.out.println("After browser started");
			
			break;
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,  TimeUnit.SECONDS);
		driver.get(baseUrl);
		return driver;
	}

	public static String getbaseUrl() {
		return baseUrl;
	}

}
