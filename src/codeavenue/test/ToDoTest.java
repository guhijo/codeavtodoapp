package codeavenue.test;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import codeavenue.pageObjects.HomePage;
import codeavenue.pageObjects.LoginPage;

public class ToDoTest extends AbstractTest {
	
	WebDriver selenium;
	static LoginPage loginPage;
	static HomePage homePage;
	private String moreThan250Chars = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
	
	@BeforeClass
	public static void initClass() {
		driver = AbstractTest.getDriver("firefox", "");
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.login(TEST_USER,TEST_PASS);	
		homePage = PageFactory.initElements(driver, HomePage.class);
	}
			
	@Before
	public void setUp(){
		homePage.clickMyTasks();
	}
	
	@AfterClass
	public static void tearDown() {
		driver.quit();
	}
		
	@Test
	public void insertEmptyTask() {
		homePage.addTask("");
		assertEquals("Empty Task.", "empty", 
				homePage.findEmptyTask());
	}	
	
	@Test
	public void insertOneCharacterTask() {
		homePage.addTask("1");
		assertEquals("Single Character Task.", "1", 
				homePage.findOneCharacterTask());
	}	
	
	@Test
	public void insertTwoCharactersTask() {
		homePage.addTask("12");
		assertEquals("Two characters Task.", "12", 
				homePage.findTwoCharactersTask());
	}	
	
	@Test
	public void insert254CharactersTask() {
		homePage.addTask(moreThan250Chars);
		assertEquals("More than 250 characters Task.", moreThan250Chars, 
				homePage.findMoreThan250CharactersTask());
	}	
	
	@Test
	public void insertEmptySubTask() {
		homePage.addTask("Task with empty subtask");
		homePage.clickManageSubTasks();
		homePage.addSubTask("", "");
		assertEquals("Empty SubTask.", "empty", 
				homePage.findEmptySubTask());
	}
	
	@Test
	public void exceptionWhenEditingSubTask() {
		homePage.addTask("Exception when editing subtask");
		homePage.clickManageSubTasks();
		homePage.addSubTask("subtask 1", "11/13/2016");
		homePage.editSubTask("Some change");
		assertEquals("Exception when editing subtask.", "Action Controller: Exception caught", 
				homePage.findEditSubTaskExceptionMessage());
		homePage.closeButton.click();
	}
	
}
