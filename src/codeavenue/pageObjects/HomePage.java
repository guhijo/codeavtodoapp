package codeavenue.pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePage {

	private WebDriver selenium;
	public String actionControllerText = "Action Controller: Exception caught";
	
	@FindBy(how = How.CLASS_NAME, using = "alert-info")
	public WebElement successLogin;
		
	@FindBy(how = How.LINK_TEXT, using = "My Tasks")
	public WebElement tasksButton;
	
	@FindBy(how = How.CLASS_NAME, using = "input-group-addon")
	public WebElement addTask;
	
	@FindBy(how = How.ID, using = "new_task")
	public WebElement newTask;
	
	@FindBy(how = How.LINK_TEXT, using = "empty")
	public WebElement emptyTask;
	
	@FindBy(how = How.LINK_TEXT, using = "empty")
	public WebElement emptySubTask;
	
	@FindBy(how = How.LINK_TEXT, using = "1")
	public WebElement oneCharTask;
	
	@FindBy(how = How.LINK_TEXT, using = "12")
	public WebElement twoCharsTask;
	
	@FindBy(how = How.LINK_TEXT, using = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss")
	public WebElement moreThan250CharsTask;
	
	@FindBy(how = How.LINK_TEXT, using = "subtask 1")
	public WebElement subTaskToBeEdited;
	
	@FindBy(how = How.CLASS_NAME, using = "editable-input")
	public WebElement subTaskEditField;
	
	@FindBy(how = How.CLASS_NAME, using = "btn-primary")
	public WebElement manageSubTaskButton;
		
	@FindBy(how = How.ID, using = "add-subtask")
	public WebElement addSubTask;
	
	@FindBy(how = How.ID, using = "new_sub_task")
	public WebElement newSubTask;
	
	@FindBy(how = How.ID, using = "dueDate")
	public WebElement newdueDate;
	
	@FindBy(how=How.XPATH, xpath ="//*[contains(text(),'" + "Action Controller: Exception caught" + "')]")
	public WebElement actionControllerException;
	
	@FindBy(how=How.XPATH, xpath ="//button[contains(.,'Close')]")
	public WebElement closeButton;
		
	public HomePage(WebDriver selenium)  {
		this.selenium = selenium;
		
	}
	
	public String getLoginMessage() {
		return successLogin.getText().trim();
	}
	
	public void clickMyTasks(){
		tasksButton.click();
	}
	
	public void clickManageSubTasks(){
		manageSubTaskButton.click();
	}
	
	public void editSubTask(String text){
		subTaskToBeEdited.click();
		subTaskEditField.clear();
		subTaskEditField.sendKeys(text);
		subTaskEditField.sendKeys(Keys.ENTER);
		try {
			   Thread.sleep(3000);
			} catch (InterruptedException e) {
			   e.printStackTrace();
			}	
		
	}
	
	public void addTask(String taskText){
		newTask.sendKeys(taskText);
		addTask.click();
	}

	public void addSubTask(String subTaskDescription, String subTaskDueDate){
		newSubTask.sendKeys(subTaskDescription);
		newdueDate.clear();
		newdueDate.sendKeys(subTaskDueDate);
		addSubTask.click();
	}
	
	public String findEmptyTask(){
		return emptyTask.getText();
	}
	
	public String findOneCharacterTask(){
		return oneCharTask.getText();
	}
	
	public String findTwoCharactersTask(){
		return twoCharsTask.getText();
	}
	
	public String findMoreThan250CharactersTask(){
		return moreThan250CharsTask.getText();
	}
	
	public String findEmptySubTask(){
		return emptySubTask.getText();
	}
	
	public String findEditSubTaskExceptionMessage(){
		return actionControllerException.getText();
	}
}
