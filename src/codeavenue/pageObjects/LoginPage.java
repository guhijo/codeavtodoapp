package codeavenue.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage {

	private WebDriver selenium;
	
	@FindBy(how = How.ID, using = "user_email")
	private WebElement userEmail;

	@FindBy(how = How.ID, using = "user_password")
	private WebElement password;
	
	@FindBy(how = How.NAME, using = "commit")
	private WebElement loginButton;
	
	public LoginPage(WebDriver selenium)  {
		this.selenium = selenium;
		
	}
	
	public void login(String loginid, String passwd) {
		userEmail.clear();
		userEmail.sendKeys(loginid);
		password.clear();
		password.sendKeys(passwd);
		loginButton.click();
	}
	
}
